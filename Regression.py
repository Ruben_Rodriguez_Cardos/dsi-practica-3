import pandas as pd
from sklearn import preprocessing, cross_validation

import numpy as np
import random

import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split

from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import AdaBoostRegressor

def loadData(filename):
    

    caracteristicas = open('Data/algebra_2008_2009_train.txt', "r") 
    caracteristicas = caracteristicas.readline()
    caracteristicas = caracteristicas.split("\t")
    caracteristicas = caracteristicas[1:]

    #Cargo los ficheros parar codificar las SubSkills, KTracedSkills y Rules
    file = open("Results/SubSkills.txt", "r") 
    lines  = file.readlines()
    file.close()
    subskills={}
    for i in range(0,len(lines)):
        line = lines[i]
        if "\n" in line:
            line = line[0:len(line)-1]
        subskills[line] = i

    if not "" in subskills.keys():
        subskills[""]=-1



    file = open("Results/KTracedSkills.txt", "r") 
    lines  = file.readlines()
    file.close()
    ktracedSkills={}
    for i in range(0,len(lines)):
        line = lines[i]
        if "\n" in line:
            line = line[0:len(line)-1]
        ktracedSkills[line] = i


    if not "" in ktracedSkills.keys():
        ktracedSkills[""]=-1

    file = open("Results/Rules.txt", "r") 
    lines  = file.readlines()
    file.close()
    rules={}
    for i in range(0,len(lines)):
        line = lines[i]
        if "\n" in line:
            line = line[0:len(line)-1]
        rules[line] = i 

    if not "" in rules.keys():
        rules[""]=-1



    #Cargo el fichero con los datos de entrenamiento
    file = open(filename, "r") 
    lines  = file.readlines()
    file.close()
    data = []

    for line in lines:
        
        #Elimino la columna row, simplmente es un indice
        aux = line.split("\t")
        aux = aux[1:]
        
        #Codificación
        
        #Obtengo Subskill, KTracedRule y Rule
        #Me quedo con la mas comun
        aux1 = aux[16].split("~~")
        aux2 = aux[18].split("~~")
        aux3 = aux[20].split("~~")

        n_aux1 = aux[17].split("~~")
        n_aux2 = aux[19].split("~~")
        n_aux3 = aux[21].split("~~")

        aux4 = ""
        n_aux4 = -1
        for i,n in zip(aux1,n_aux1):
            if n != "":
                if int(n) > int(n_aux4):
                    n_aux4 = n
                    aux4 = i
            else:
                aux4=""
        aux[16] = int(subskills[aux4])


        aux4 = ""
        n_aux4 = -1
        for i,n in zip(aux2,n_aux2):
            if n != "":
                if int(n) > int(n_aux4):
                    n_aux4 = n
                    aux4 = i
            else:
                aux4=""
        aux[18] = int(ktracedSkills[aux4])
        
        
        aux4 = ""
        n_aux4 = -1
        for i,n in zip(aux3,n_aux3):
            if n != "" and n!="\n":
                if int(n) > int(n_aux4):
                    n_aux4 = n
                    aux4 = i
                else:
                    aux4=""
            else:
                aux4=""
        aux[20] = int(rules[aux4])

        #Cambio de tipo de datos a los datos numericos
        aux[3] = int(aux[3])
        aux[13] = int(aux[13])
        aux[14] = int(aux[14])
        aux[15] = int(aux[15])

        data.append(aux)
        

    data_train = []
    data_test = []

    for i in data:
        if random.random() > 0.4:
            data_train.append(i)
        else:
            data_test.append(i)


    data_train = pd.DataFrame.from_records(data_train ,columns=caracteristicas)
    data_test = pd.DataFrame.from_records(data_test ,columns=caracteristicas)

    data_train = normalize(data_train)
    data_test = normalize(data_test)

    return data_train, data_test

def normalize(df):
    result = df.copy()
    for feature_name in df.columns:
        features_to_normalize = ['Problem View','Incorrects','Hints','Corrects','KC(SubSkills)','KC(KTracedSkills)','KC(Rules)']
        if feature_name in features_to_normalize:
            max_value = df[feature_name].max()
            min_value = df[feature_name].min()
            result[feature_name] = (df[feature_name] - min_value) / (max_value - min_value)
    return result

def doRegresion(data_train,data_test,filename):
    #Cargo los nuevos datos
    
    X_train = data_train[['Problem View','Incorrects','Hints','Corrects','KC(SubSkills)','KC(KTracedSkills)','KC(Rules)']]
    Y_train = data_train['Correct First Attempt']

    X_test = data_test[['Problem View','Incorrects','Hints','Corrects','KC(SubSkills)','KC(KTracedSkills)','KC(Rules)']]

    #ADABOOST Regresión
    rng = np.random.RandomState(1)
    regr = AdaBoostRegressor(DecisionTreeRegressor(max_depth=4),n_estimators=300, random_state=rng)

    regr.fit(X_train, Y_train)

    # Predict
    Y_test = regr.predict(X_test)
    file = open(filename,"w")
    for i in Y_test:
        file.write(str(i)+"\n")
    file.close()

    #Muestro los resultados de forma grafica
    rangos = [0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
    dic_resultados={}
    for i in rangos:
        dic_resultados[i]=0

    for i in Y_test:
        cont = 0
        aux = i
        for j in range(0,11):
            aux = aux - 0.1
            if aux < 0.0:
                break
            cont=cont+1
        dic_resultados[cont*0.1] = dic_resultados[cont*0.1]+1




    plt.bar(range(len(dic_resultados)), list(dic_resultados.values()), align='center')
    plt.xticks(range(len(dic_resultados)), list(dic_resultados.keys()))
    plt.ylabel('Numero de pasos')
    plt.xlabel('Probabilidad de acierto al primer intento (intervalos)')
    plt.savefig('Results/Resultados.png')
    plt.show()


def main():
    data_train,data_test = loadData('Data/SHORT_algebra_2008_2009_train.txt')
    doRegresion(data_train,data_test,"Results/Results.txt")

if __name__ == '__main__':
	main()
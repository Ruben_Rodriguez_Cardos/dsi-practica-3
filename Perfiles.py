import pandas as pd

def main():

	file = open('Data/algebra_2008_2009_train.txt', "r") 
	lines  = file.readlines()
	file.close()
	lines = lines[1:]

	#543 Skills
	skills=[]

	#Diccionario alumno/skills
	student_skills={}

	#Diccionario problema_paso/Skill
	problem_skill={}


	print("Looking for skills...")
	for line in lines:

		#Alumno/Skills

		student = line.split("\t")[1]
		correct = int(line.split("\t")[16])
		skills = line.split("\t")[17].split("~~")

		problema = line.split("\t")[3]
		paso = line.split("\t")[5]
		problema_paso=problema+"_"+paso

		#Alumnos/Skills
		if correct > 0:

			#Si el alumno no esta en el diccionario lo añado
			if student not in student_skills.keys():
				student_skills[student] = skills
			else:#El alumno ya esta en el diccionario
				aux = student_skills[student]
				for skill in skills:
					if skill not in aux:
						aux = aux + [skill]
				student_skills[student] = aux

		#Problema-paso/Skill
		if problema_paso not in problem_skill.keys():
				problem_skill[problema_paso] = skills
		else:#El alumno ya esta en el diccionario
			aux = problem_skill[problema_paso]
			for skill in skills:
				if skill not in aux:
					aux = aux + [skill]
			problem_skill[problema_paso] = aux




	#Busco skills de cada alumno, si supera un paso se supone que tiene dicha skill
	print("Saving skills...")
	file = open('Results/StudentsSkills.txt', "w") 
	for key, value in student_skills.items():
		aux = ""
		for i in value:
			aux=aux+i+"~~"
		aux = aux[:len(aux)-2]
		file.write(key+"\t"+aux+"\n")
	file.close()

	file = open('Results/ProblemsSkills.txt', "w") 
	for key, value in problem_skill.items():
		aux = ""
		for i in value:
			aux=aux+i+"~~"
		aux = aux[:len(aux)-2]
		file.write(key+"\t"+aux+"\n")
	file.close()	



	

	

if __name__ == "__main__":
	main() 
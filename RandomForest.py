import pandas as pd
from sklearn import preprocessing, cross_validation

import numpy as np
import random

import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split

from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import AdaBoostRegressor

#Los datos existentes se dividen en un 60,40 como datos de entrenamiento y datos de test


def loadData(filename):
    

    caracteristicas = open('Data/algebra_2008_2009_train.txt', "r") 
    caracteristicas = caracteristicas.readline()
    caracteristicas = caracteristicas.split("\t")
    caracteristicas = caracteristicas[1:]

    #Cargo los ficheros parar codificar las SubSkills, KTracedSkills y Rules
    file = open("Results/SubSkills.txt", "r") 
    lines  = file.readlines()
    file.close()
    subskills={}
    for i in range(0,len(lines)):
        line = lines[i]
        if "\n" in line:
            line = line[0:len(line)-1]
        subskills[line] = i

    if not "" in subskills.keys():
        subskills[""]=-1



    file = open("Results/KTracedSkills.txt", "r") 
    lines  = file.readlines()
    file.close()
    ktracedSkills={}
    for i in range(0,len(lines)):
        line = lines[i]
        if "\n" in line:
            line = line[0:len(line)-1]
        ktracedSkills[line] = i


    if not "" in ktracedSkills.keys():
        ktracedSkills[""]=-1

    file = open("Results/Rules.txt", "r") 
    lines  = file.readlines()
    file.close()
    rules={}
    for i in range(0,len(lines)):
        line = lines[i]
        if "\n" in line:
            line = line[0:len(line)-1]
        rules[line] = i 

    if not "" in rules.keys():
        rules[""]=-1



    #Cargo el fichero con los datos de entrenamiento
    file = open(filename, "r") 
    lines  = file.readlines()
    file.close()
    data = []

    for line in lines:
        
        #Elimino la columna row, simplmente es un indice
        aux = line.split("\t")
        aux = aux[1:]
        
        #Codificación
        
        #Obtengo Subskill, KTracedRule y Rule
        #Me quedo con la mas comun
        aux1 = aux[16].split("~~")
        aux2 = aux[18].split("~~")
        aux3 = aux[20].split("~~")

        n_aux1 = aux[17].split("~~")
        n_aux2 = aux[19].split("~~")
        n_aux3 = aux[21].split("~~")

        aux4 = ""
        n_aux4 = -1
        for i,n in zip(aux1,n_aux1):
            if n != "":
                if int(n) > int(n_aux4):
                    n_aux4 = n
                    aux4 = i
            else:
                aux4=""
        aux[16] = int(subskills[aux4])


        aux4 = ""
        n_aux4 = -1
        for i,n in zip(aux2,n_aux2):
            if n != "":
                if int(n) > int(n_aux4):
                    n_aux4 = n
                    aux4 = i
            else:
                aux4=""
        aux[18] = int(ktracedSkills[aux4])
        
        
        aux4 = ""
        n_aux4 = -1
        for i,n in zip(aux3,n_aux3):
            if n != "" and n!="\n":
                if int(n) > int(n_aux4):
                    n_aux4 = n
                    aux4 = i
                else:
                    aux4=""
            else:
                aux4=""
        aux[20] = int(rules[aux4])

        data.append(aux)
        

    data_train = []
    data_test = []

    for i in data:
        if random.random() > 0.4:
            data_train.append(i)
        else:
            data_test.append(i)


    data_train = pd.DataFrame.from_records(data_train ,columns=caracteristicas)
    data_test = pd.DataFrame.from_records(data_test ,columns=caracteristicas)

    print(data_train)
    print(data_test)

    return data_train, data_test


def doRandomForest(data,title): 
    
    #Prueba

    X = data[['Problem View','Incorrects','Hints','Corrects','KC(SubSkills)','KC(KTracedSkills)','KC(Rules)']]
    
    y = data['Correct First Attempt']

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
    regressor = RandomForestRegressor(n_estimators= 10, criterion='mae', random_state=0)
    regressor.fit(X_train, y_train)
    y_pred = regressor.predict(X_test)
    mae = mean_absolute_error(y_test,y_pred)
    print("\nError Measure ", mae)
    
    # FEATURE RELEVANCIES
    print ('Feature Relevances')
    rf=pd.DataFrame({'Attributes': features , 'Random Forests':regressor.feature_importances_})
    rf=rf.sort_values(by='Random Forests', ascending=0)
    title=title+".csv"
    rf.to_csv(title, index=False)
    return rf

def main():
    data_train,data_test = loadData('Data/SHORT_algebra_2008_2009_train.txt')
    rf=doRandomForest(data_train,"Results/RF")

if __name__ == '__main__':
	main()
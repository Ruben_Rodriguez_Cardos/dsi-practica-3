import pandas as pd

def loadData():
	#Obtengo las columnas del Dataframe
	file = open('Data/algebra_2008_2009_train.txt', "r") 
	lines  = file.readlines()
	file.close()

	lines= lines[1:]

	return lines

	


def getAllSubSkills(lines):
	data = []

	for line in lines:

		#Elimino la columna row, simplmente es un indice
		aux = line.split("\t")
		aux = aux[1:]

		aux = aux[16].split("~~")
		for i in aux:
			if i not in data:
				data.append(i)

	data.remove("")

	file = open('Results/SubSkills.txt', "w") 
	for i in data:
		file.write(i+"\n")
	file.close()

def getAllKTracedSkills(lines):
	data = []

	for line in lines:

		#Elimino la columna row, simplmente es un indice
		aux = line.split("\t")
		aux = aux[1:]

		aux = aux[18].split("~~")
		for i in aux:
			if i not in data:
				data.append(i)

	data.remove("")

	file = open('Results/KTracedSkills.txt', "w") 
	for i in data:
		file.write(i+"\n")
	file.close()

def getAllRules(lines):
	data = []

	for line in lines:

		#Elimino la columna row, simplmente es un indice
		aux = line.split("\t")
		aux = aux[1:]

		aux = aux[20].split("~~")
		for i in aux:
			if i not in data:
				data.append(i)

	data.remove("")

	file = open('Results/Rules.txt', "w") 
	for i in data:
		file.write(i+"\n")
	file.close()


def getAllProblemHierarchy(lines):
	data = []

	for line in lines:

		#Elimino la columna row, simplmente es un indice
		aux = line.split("\t")
		aux = aux[1:]

		aux = aux[2]
		if aux not in data:
			data.append(aux)

	data.remove("")

	file = open('Results/ProblemHierarchy.txt', "w") 
	for i in data:
		file.write(i+"\n")
	file.close()
	
def getAllProblemName(lines):
	data = []

	for line in lines:

		#Elimino la columna row, simplmente es un indice
		aux = line.split("\t")
		aux = aux[1:]
		aux = aux[2]
		if aux not in data:
			data.append(aux)

	file = open('Results/ProblemName.txt', "w") 
	for i in data:
		file.write(i+"\n")
	file.close()

def getAllStudents(lines):
	data = []

	for line in lines:

		#Elimino la columna row, simplmente es un indice
		aux = line.split("\t")

		aux = aux[0]
		if aux not in data:
			data.append(aux)

	file = open('Results/Students.txt', "w") 
	for i in data:
		file.write(i+"\n")
	file.close()



def main():
	lines=loadData()
	#getAllSubSkills(lines)
	#getAllKTracedSkills(lines)
	#getAllRules(lines)
	#getAllProblemHierarchy(lines)
	#getAllProblemName(lines)
	getAllStudents(lines)

	
	


	

if __name__ == "__main__":
	main() 
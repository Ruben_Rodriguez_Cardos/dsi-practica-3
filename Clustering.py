from sklearn.mixture import GaussianMixture
from sklearn import metrics
import matplotlib.pyplot as plt
import random
import os
import pandas as pd

import numpy as np
import itertools
from scipy import linalg
from collections import Counter

#BIC
def BIC(data):

	#Metodo de la documentación de Sckit-learn
	#http://scikit-learn.org/stable/auto_examples/mixture/plot_gmm_selection.html

	lowest_bic = np.infty
	bic = []
	n_components_range = range(2, 11)
	cv_types = ['spherical', 'tied', 'diag', 'full']
	for cv_type in cv_types:
	    for n_components in n_components_range:
	        # Fit a Gaussian mixture with EM
	        gmm = GaussianMixture(n_components=n_components,
	                                      covariance_type=cv_type)
	        gmm.fit(data)
	        bic.append(gmm.bic(data))
	        if bic[-1] < lowest_bic:
	            lowest_bic = bic[-1]
	            best_gmm = gmm

	bic = np.array(bic)
	clf = best_gmm
	bars = []

	# Plot the BIC scores
	winner = str(clf)
	str2 = "n_components="
	pos_winner = winner.find(str2)
	winner = int(winner[pos_winner+len(str2)])
	return winner




# Carga de los datos
def loaddata(filename):
    
    print("Loading the data...")
    
    #Resultados
    data = pd.read_csv(filename)
    return data


def saveResult(data,filename):
	print("Saving results to a file...")
		
	#Si el directorio no existe lo creo
	if not os.path.exists("Results"):
		os.makedirs("Results")

	file = open(filename, "w")
	file.write("Id\tUnidad\tProblema\tPaso\tNVecesVisto\tIncorrectos\tPistas\tCorrectos\tCluster\t\n")
	for i in data:
		aux =""
		for j in i:
			aux=aux+str(j)+"\t"
		aux=aux+"\n"
		file.write(aux)
	file.close()

def doClustering(data):
    labels = [0 for x in range(len(data))]
    silhouettes = []

	#Saco los datos necesarios del dataframe (data)
    colsToDrop=["Id_Alumno","STD_NVecesVisto","STD_Incorrectos","STD_Pistas","STD_Correctos"]
    aux=data.drop(columns=colsToDrop)

    #ESTUDIO DE SI ES NECESARIO UNA NORMALIZACION
    #PARA ELLO VEO LAS DIFERENTES ESCALAS
    #print("Intervalo NVecesVisto: ",data["NVecesVisto"].min()," - ",data["NVecesVisto"].max())
    #print("Intervalo Incorrectos: ",data["Incorrectos"].min()," - ",data["Incorrectos"].max())
    #print("Intervalo Pistas: ",data["Pistas"].min()," - ",data["Pistas"].max())
    #print("Intervalo Correctos: ",data["Correctos"].min()," - ",data["Correctos"].max())
    
    #La variación es pequeña
    #Intervalo NVecesVisto:  1.0  -  2.727272727272727
	#Intervalo Incorrectos:  0.0  -  2.3846153846153846
	#Intervalo Pistas:  0.0  -  1.0
	#Intervalo Correctos:  0.0  -  1.4146341463414631



    #METODO BIC
    print("Execute BIC method...")
    """bic_scores = []
    for i in range(1,50):
    	bic_scores.append(BIC(aux))

    cnt = Counter(bic_scores)
    i = cnt.most_common(1)[0][0]"""

    print("Starting the clustering...")
    for i in range(2, 11):
        print("Try with i: "+str(i))
        em = GaussianMixture(n_components=i,covariance_type='full', init_params='kmeans')
        em.fit(aux)
        labels =  em.predict(aux)
        #silhouettes.append(metrics.silhouette_score(aux, labels))
        #print("Silhouette: "+str(metrics.silhouette_score(aux, labels)))
        silhouettes.append(em.bic(aux))
        print("Silhouette: "+str(em.bic(aux)))

    n = -1
    n_min = silhouettes[0]
    for i in range(0,len(silhouettes)):
    	if silhouettes[i]<n_min:
    		n = i
    		n_min = silhouettes[i] 

	#Se debe sumar dos a n, puesto que la primera posicion de la lista representa i:2
    n=n+2
	
	

    # Plot Silhouette
    plt.plot(range(2,11), silhouettes , marker='o')
    plt.xlabel('Number of clusters')
    plt.ylabel('Silohouette')
    plt.show()

    #Se obtiene i con el metodo BIC (6 parece ser n mas comun)
    em = GaussianMixture(n_components=n,covariance_type='full', init_params='kmeans')
    em.fit(aux)
    labels =  em.predict(aux)

    #Añado los labels al dataframe
    aux["Cluster"]=labels
    return aux

def studyResults(df,filename):

    #Veo los datos por cluster
    n_clusters=df['Cluster'].max()

    #unidades = df["Unidad"].unique()

    file = open(filename, "w")

    file.write("\n\n---Clustering---\n")

    for i in range(0,n_clusters+1):

        #Me quedo con los registros del cluster
        aux = df.loc[df['Cluster'] == i]
        
        print("Cluster: "+str(i))
        file.write("Cluster: "+str(i)+"\n")
        print("Nº elementos en el cluster: ", aux["Cluster"].count())
        file.write("Nº elementos en el cluster: "+ str(aux["Cluster"].count())+"\n")
        print("Nº medio de veces vistas: ",aux["NVecesVisto"].mean())
        file.write("Nº medio de veces vistas: "+str(aux["NVecesVisto"].mean())+"\n")
        print("Nº medio de intentos incorrectos: ",aux["Incorrectos"].mean())
        file.write("Nº medio de intentos incorrectos: "+str(aux["Incorrectos"].mean())+"\n")
        print("Nº medio de pistas: ",aux["Pistas"].mean())
        file.write("Nº medio de pistas: "+str(aux["Pistas"].mean())+"\n")
        print("Nº medio de correctos: ",aux["Correctos"].mean())
        file.write("Nº medio de correctos: "+str(aux["Correctos"].mean())+"\n\n")
        print()

    file.close()


def main():
    #Carga de los datos
    data = loaddata('Data/STUDENTS_algebra_2008_2009_train.csv') 

    results_clustering = doClustering(data)

    #Guardo los resultados	
    results_clustering.to_csv("Results/STUDENTS-WITH-CLUSTERS_algebra_2008_2009_train.csv", index=False)

    #Estudio de los Resultados
    studyResults(results_clustering,"Results/Clustering_study.txt")
    




if __name__== "__main__":
	main()
*SCRIPTS*

Agrupacion.py -> Agrupación de los registros del 20% de los alumnos, calculando la media y la desviación tipica.

Clustering.py -> Clustering de los datos ya agrupados por alumnos

GetData.py -> Codificacion de las caracteristicas no numericas, Subskills, ktracedskills,etc...

Infoy.py -> Información varia sobre los datos.

Mapeo.py ->Redución del total de los datos a unicamente los registros del 20% de los alumnos.

Perfiles.py -> Obtención de los perfiles de cada alumno, las skills de cada alumno.

RandomForest.py -> Medición del peso de las caracteristicas para la regresion.

Regresion.py -> Creacion del modelo de dtaos y predicción.

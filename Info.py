import pandas as pd

def allRowsInfo():
	#Obtengo las columnas del Dataframe
	file = open('Data/algebra_2008_2009_train.txt', "r") 
	caracteristicas  = file.readline()
	file.close()
	
	#Elimino la columna row, simplmente es un indice
	caracteristicas = caracteristicas.split("\t")
	caracteristicas = caracteristicas[1:]
	

	#Leo el fichero corto
	file = open('Data/SHORT_algebra_2008_2009_train.txt', "r") 
	lines = file.readlines()
	file.close()

	for line in lines:
		data=line

		#Elimino la columna row, simplmente es un indice
		data = data.split("\t")
		data = data[1:]

		#Muestro la informacion
		i = 1
		for key, item in zip(caracteristicas,data):
			print(i,key,item)
			i=i+1

		print("\n")

def numberOfInfo():
	print("Loading the file...")
	file = open('Data/SHORT_algebra_2008_2009_train.txt', "r") 
	aux = file.readlines()
	file.close()
	print("File loaded...")
	  
	data = []
	for i in range(1,len(aux)):
	  row = aux[i].split('\t')
	  data.append([row[1],row[3],int(row[4]),int(row[14]), int(row[15]), int(row[16])])
	    
	df = pd.DataFrame.from_records(data ,columns=["Id_Alumno","Id_Problema","NVecesVisto","Incorrectos","Pistas","Correctos"])

	problemas = df["Id_Problema"].unique()
	alumnos = df["Id_Alumno"].unique()
	print("Numero de problemas: ",len(problemas))
	print("Numero de alumnos: ",len(alumnos))

def testRowsInfo():

	#Obtengo las columnas del Dataframe
	file = open('Data/algebra_2008_2009_test.txt', "r") 
	lines  = file.readlines()
	file.close()

	caracteristicas = lines[0].split("\t")
	caracteristicas = caracteristicas[1:]
	lines = lines[1:]

	data = []

	for line in lines:
		

		#Elimino la columna row, simplmente es un indice
		aux = line.split("\t")
		aux = aux[1:]
		data.append(aux)

		#Muestro la informacion
		"""i = 1
		for key, item in zip(caracteristicas,data):
			print(i,key,item)
			i=i+1

		print("\n")"""

	df = pd.DataFrame.from_records(data ,columns=caracteristicas)
	colsToDrop=[
    "Step Start Time",
    "First Transaction Time",
    "Correct Transaction Time",
    "Step End Time",
    "Step Duration (sec)",
    "Correct Step Duration (sec)",
    "Error Step Duration (sec)",
    "Correct First Attempt",
    "Incorrects",
    "Hints",
    "Corrects"]
	df=df.drop(columns=colsToDrop)
	print(df)
	print(df.columns)


def main():
	#Info
	allRowsInfo()
	#numberOfInfo()
	#testRowsInfo()
	
	


	

if __name__ == "__main__":
	main() 
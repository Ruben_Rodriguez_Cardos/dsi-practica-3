Identifying units
Define Variable
Write expression, any form
Using simple numbers
Using large numbers
Write expression, positive intercept
Write expression, negative slope
Write Expression, mx+b
Entering a given
Enter given, reading words
Enter given, implied amount
Find Y, any form
Find Y, negative slope
Enter given, reading numerals
Convert unit, standard
Compare units
Enter unit conversion
Select form of one with numerator of one
Enter numerator of converted unit
Enter denominator of converted unit
Convert time units
Convert small number units
Convert integer units
Convert within system
Select form of one with denominator of one
Convert length units
Convert decimal units less than one
Convert volume units
Convert weight units
Convert decimal units greater than one
Convert large number units
Convert area units
Convert across system
Convert medium number units
Write expression, zero intercept
Write expression, positive slope
Write expression, simple
Using small numbers
Find Y, Simple
Find X, any form
Find X, Simple
Labelling the axes
Changing axis bounds
Changing axis intervals
Correctly placing points
Find Y, positive slope
Find X, positive slope
Convert unit, offset
Convert unit, mixed
Write expression, negative intercept
[SkillRule: Select Multiply/Divide, nested; {MT; MT no fraction coeff}]
[SkillRule: Select Perform Multiplication; {MT rational no fraction coeff; MT no fraction coeff}]
[SkillRule: Combine like terms, no var; CLT]
[SkillRule: Select Combine Terms; {CLT; CLT 2plus terms same denom, no frac elim; CLT, no lcd}]
[SkillRule: Select Eliminate Parens; {CLT nested; CLT nested, parens; Distribute Mult right; Distribute Mult left; Distribute subex}]
[SkillRule: Eliminate Parens; {CLT nested; CLT nested, parens; Distribute Mult right; Distribute Mult left; (+/-x +/-a)/b=c, mult; (+/-x +/-a)*b=c, div; [var expr]/[const expr] = [const expr], multiply; Distribute Division left; Distribute Division right; Distribute both mult left; Distribute both mult right; Distribute both divide left; Distribute both divide right; Distribute subex}]
[SkillRule: Select Multiply; {MT; MT no fraction coeff}]
combine-like-terms-whole-sp
perform-mult-row2-sp
perform-mult-whole-sp
combine-like-terms-sp
[SkillRule: Done?; {doneleft; doneright; doneleft, no menu; doneright, nomenu; done no solution; Done No Solution, domain exception; Done No Solution, range exception; done infinite solutions; done expr, standard form; done expr, standard form, no menu; done rational expr, factored form, no menu; simp radical donenomenu; quad form general, donenomenu left; quad form general, donenomenu right; quad form factored, donenomenu left; quad form factored, donenomenu right; Quad form vertex, donenomenu left; Quad form vertex, donenomenu right; rational doneleft, nomenu; rational doneright, nomenu}]
[SkillRule: Consolidate vars, no coeff; CLT]
[SkillRule: Remove constant; {ax+b=c, positive; ax+b=c, negative; x+a=b, positive; x+a=b, negative; [var expr]+[const expr]=[const expr], positive; [var expr]+[const expr]=[const expr], negative; [var expr]+[const expr]=[const expr], all; Combine constants to right; Combine constants to left; a-x=b, positive; a/x+b=c, positive; a/x+b=c, negative}]
[SkillRule: Isolate variable term in linear equation; {[var expr]+[const expr]=[const expr], positive; [var expr]+[const expr]=[const expr], negative; [var expr]+[const expr]=[const expr], all; x+a=b, negative; x+a=b, positive; ax+b=c, positive; ax+b=c, negative; ax-b=c, negative; a/x+b=c, positive; a/x+b=c, negative; a-x=b, positive}]
[SkillRule: Isolate variable in linear equation; {[var expr]+[const expr]=[const expr], positive; [var expr]+[const expr]=[const expr], negative; [var expr]+[const expr]=[const expr], all; x+a=b, negative; x+a=b, positive}]
[SkillRule: Isolate positive; x+a=b, positive]
[SkillRule: Isolate negative; x+a=b, negative]
[SkillRule: Remove coefficient; {ax+b=c, divide; ax=b; [const expr]*[var fact] + [const expr] = [const expr], divide; [var expr]*[const expr] = [const expr], divide; a/b*x=c; a/b*x=c, reciprocal; ax/b=c, reciprocal; ax/b=c; x/a=b; ax=b; (+/-x +/-a)/b=c, mult; a=x*(b+c); a=x*(b-c); a=x*(b*c+d); x/a+b=c, multiply; [var expr]/[const expr] = [const expr], multiply}]
[SkillRule: Remove positive coefficient; {ax+b=c, divide; ax=b; [const expr]*[var fact] + [const expr] = [const expr], divide; [var expr]*[const expr] = [const expr], divide; a/b*x=c; a/b*x=c, reciprocal; ax/b=c, reciprocal; ax/b=c; x/a=b; ax=b; (+/-x +/-a)/b=c, mult; a=x*(b+c); a=x*(b-c); a=x*(b*c+d); x/a+b=c, multiply; [var expr]/[const expr] = [const expr], multiply}]
[SkillRule: Select Simplify Fraction; {RF rational ratio; rf expression; RF left; RF right}]
[SkillRule: Remove negative coefficient; {ax+b=c, divide; ax=b; [const expr]*[var fact] + [const expr] = [const expr], divide; [var expr]*[const expr] = [const expr], divide; a/b*x=c; a/b*x=c, reciprocal; ax/b=c, reciprocal; ax/b=c; x/a=b; ax=b; (+/-x +/-a)/b=c, mult; a=x*(b+c); a=x*(b-c); a=x*(b*c+d); x/a+b=c, multiply; [var expr]/[const expr] = [const expr], multiply}]
[SkillRule: Isolate variable in linear equation; {[var expr]/[const expr] = [const expr], multiply; ax=b}]
[SkillRule: Multiply/Divide; [Typein Skill: {Remove coefficient; Variable in denominator}]]
simplify-fractions-sp
[SkillRule: Make variable positive; {ax+b=c, divide; ax=b; [const expr]*[var fact] + [const expr] = [const expr], divide; [var expr]*[const expr] = [const expr], divide; a/b*x=c; a/b*x=c, reciprocal; ax/b=c, reciprocal; ax/b=c; x/a=b; ax=b; (+/-x +/-a)/b=c, mult; a=x*(b+c); a=x*(b-c); a=x*(b*c+d); x/a+b=c, multiply; [var expr]/[const expr] = [const expr], multiply}]
Using difficult numbers
[SkillRule: ax+b=c, negative; ax+b=c, negative]
[SkillRule: Consolidate vars with coeff; CLT]
Enter ratio label in numerator
Enter ratio label in denominator
Enter given ratio numerator
Enter given ratio denominator
Identify that a fraction can/cannot be simplified
Enter ratio label to left of "to"
Enter ratio label to right of "to"
Enter ratio quantity to left of "to"
Enter ratio quantity to right of "to"
Enter ratio using a colon
Calculate ratio numerator from given
Enter ratio label to left of colon
Enter ratio label to right of colon
Enter ratio quantity to left of colon
Enter ratio quantity to right of colon
Calculate ratio denominator from given
Enter rate label in numerator
Enter rate label in denominator
Enter given rate numerator
Enter given rate denominator
Enter rate label to left of "per"
Enter rate label to right of "per"
Calculate unit rate
Compare unit rates
Enter proportion label in numerator
Enter proportion label in denominator
Enter numerator of given unit rate in proportion
Enter denominator of given unit rate in proportion
Enter given part in proportion
Enter total in proportion with variable
Choose operator in fractional identity
Enter denominator of form of 1
Enter numerator of form of 1
Calculate total in proportion with fractions
Enter Calculated value of rate
Enter numerator of given rate in proportion
Enter denominator of given rate in proportion
Enter given total in proportion
Enter part in proportion with variable
Calculate part in proportion with fractions
Enter numerator of given fraction in proportion
Enter denominator of given fraction in proportion
Enter Calculated value of ratio
Enter second mean in equation
Enter second extreme in equation
Enter first extreme in equation
Enter first mean in equation
Calculate product of means or extremes
Find means and extremes solution using simple numbers
Find means and extremes solution using difficult numbers
[SkillRule: Simplify fractions; {rf expression}]
[SkillRule: Isolate y variable; {[var expr]+[const expr]=[const expr], positive; [var expr]+[const expr]=[const expr], negative; [var expr]+[const expr]=[const expr], all; [var expr]*[const expr] = [const expr], divide; [var expr]/[const expr] = [const expr], multiply}]
Write simple inequality in contextual problem
Represent openpoint on numberline
Represent ray on numberline
Identify when finished with numberline
Identify visible non-inflection point is in solution set
Identify invisible non-inflection point is not in solution set
Identify visible non-inflection point is not in solution set
Represent closedpoint on numberline
Represent inequality in symbolic problem
Identify inflection point in solution set
Identify inflection point not in solution set
Identify invisible non-inflection point is in solution set
Write simple inequality in verbal problem
Write compound inequality in verbal problem
Join inequalities with AND
Join inequalities with OR
Write compound inequality in symbolic problem
Represent segment on numberline
Identify type of solution
[SkillRule: Distribute; {Distribute Mult right; Distribute Mult left; Distribute -1 to remove parens subex; Distribute subex}]
distribute-sp
Plot point between minor tick marks - integer major/minor
Plot non-terminating proper fraction
Plot percent
Plot point on minor tick mark - fractional major/minor
Plot decimal - tenths
Plot point on major tick mark - fractional major/minor
Plot whole number
Plot point on minor tick mark - integer major/minor
Plot non-terminating mixed number
Plot terminating proper fraction
Plot point on minor tick mark - integer major fractional minor
Plot terminating improper fractions
Plot point between minor tick marks - integer major fractional minor
Plot pi
Plot terminating mixed number
Plot point between minor tick marks - fractional major/minor
Plot decimal - thousandths
Plot perfect radical
Plot point on major tick mark - integer major any minor
Plot imperfect radical
Plot non-terminating improper fraction
Plot decimal - hundredths
Write absolute value equation
Classify equation as having two solutions
Classify equation as having one solution
Classify equation as having no solution
Solve equation having one solution
Solve equation having two solutions
Write absolute value inequality
Choose form of compound inequality
Classify inequality as having a line segment
Classify inequality as having two rays
Classify inequality as having all real numbers except a single point
Classify inequality as having all real numbers
Plot solution of less than on number line
Classify inequality as having no solution
Plot solution of less than or equal to on number line
Classify inequality as having a single point
Write expression, negative one slope
Setting the y-intercept
Setting the slope
Entering the y-intercept
Entering the slope
Bogus skill
Find slope-intercept form from point-slope form
Enter given point
Enter given slope from context
Enter given point from context
Enter point-slope form from given point and slope
Enter given intercept from context
Enter point-slope form from given intercept and slope
Enter given slope
Find slope of decreasing line
Find slope using points in quadrant 3
Find slope using points across quadrants
Find slope of increasing line
Find slope of horizontal line
Find slope using points in quadrant 1
Find slope using points in quadrants 2 or 4
Find slope from two given points
Enter point-slope form from two given points
Enter slope from equation of parallel line
Enter slope from given slope-intercept form
Enter given slope-intercept form
Enter point-slope form from calculated point and slope
Find slope-intercept form from parallel line
Enter slope from equation of perpendicular line
Find slope-intercept form from perpendicular line
Variable-Expression-Gla:Student-Modeling-Analysis
Product-Expression-Gla:Student-Modeling-Analysis
Sum-Expression-Gla:Student-Modeling-Analysis
Proportional-Constant-Expression-Gla:Student-Modeling-Analysis
Describe-Proportional-Constant-Gla:Student-Modeling-Analysis
Cterm-Expression-Gla:Student-Modeling-Analysis
Describe-Cterm-Gla:Student-Modeling-Analysis
Describe-Variable-Gla:Student-Modeling-Analysis
Describe-Product-Gla:Student-Modeling-Analysis
Describe-Sum-Gla:Student-Modeling-Analysis
Full-Expression-Gla:Student-Modeling-Analysis
[SkillRule: Extract to consolidate vars; {factor; factorb; factorc; factord}]
factor-sp
[SkillRule: Consolidate vars, any; {Combine variables to right, sub; Combine variables to right, add; Combine variables to right, gen; Combine variables to right; Combine variables to left, add; Combine variables to left, sub; Combine variables to left, gen; ax+b=cx; ax+b=cx, move left; ax+b=cx+d, pos; ax+b=cx+d, neg; ax+b=cx+d, move right; ax+b=cx+d, move two; ax+b=cx+d, move two2; x+b=cx+d, pos; x+b=cx+d, neg; x+b=cx+d, move right; x+b=cx+d, move two; x+b=cx+d, move two2; ax+b=x+d, pos; ax+b=x+d, neg; ax+b=x+d, move right; ax+b=x+d, move two; ax+b=x+d, move two2; x+b=x+d, pos; x+b=x+d, neg; x+b=x+d, move right; x+b=x+d, move two; x+b=x+d, move two2}]
[SkillRule: Add/Subtract; [Typein Skill: {Isolate positive; Isolate negative; Remove constant; Consolidate vars, no coeff; Consolidate vars with coeff; Consolidate vars, any}]]
[SkillRule: Variable in denominator; {a/x=[anything]; a/x=b; a/x=b, sophisticated}]
[SkillRule: done single solution; {doneleft; doneright}]
Labelling point of intersection
Find X, negative slope
Write expression, ratio
Write expression, positive one slope
Positive Constants, SIF
Entering y-intercept, SIF
Entering slope, SIF
Placing coordinate point
Negative Constant, SIF
Finding the intersection, SIF
Positive Constants, GLF
Entering y-intercept, GLF
Entering slope, GLF
Finding the intersection, Mixed
Negative Constant, GLF
Finding the intersection, GLF
Entering a point
Shading greater than
Including the line when shading
Shading SIF equation with positive slope
Shading less than
Excluding the line when shading
Shading SIF equation with negative slope
Shading GLF equation with negative slope
Shading GLF equation with positive slope
Finding the region
Write expression, quadratic
Entering a given linear value
Entering a computed linear value
Entering a computed quadratic value
Entering a given quadratic value
Construct square from side length
Write given whole number as square
Find positive square root
Find negative square root
Construct square from area
Write given square as whole number
Choose non-perfect square
Identify lower perfect square
Identify upper perfect square
Identify lower square root as whole number
Identify upper square root as whole number
Identify approximation as lower half
Identify lower square root as tenths decimal
Identify upper square root as tenths decimal
Enter square root to tenths place
qft-num1-sp
qft-num2-sp
qft-den-sp
[SkillRule: Separate roots; {separate plus/minus; set factors equal to 0, left; set factors equal to 0, right}]
[SkillRule: Multiply radicals; {MT no fraction coeff}]
[SkillRule: Apply Exponent; {Expand Exponents; eval radical; Expand Exponents, radical}]
factor-quadratic-sp
[SkillRule: Set Factors equal to zero; {set factors equal to 0, left; set factors equal to 0, right}]
Identify no more factors
Write base of exponential from given whole number
Write positive exponent of exponential from given number
Write base of exponential from given whole number as product
Write positive exponent of exponential from given number as product
Write whole number from given number as product
Write whole number from given exponential
Write base of exponential from given fraction
Write negative exponent of exponential from given number
Write fraction from given exponential
Write base of exponential from given fraction as product
Write negative exponent of exponential from given number as product
Write fraction from given number as product
Write whole number multiplier from given scientific notation
Write positive power of ten from given scientific notation
Write number from positive exponent
Write number from whole number multiplier
Write whole number multiplier from given number
Write positive power of ten from given number
Write whole number multiplier of scientific notation
Write positive exponent of scientific notation
Write decimal multiplier from given number
Write decimal multiplier of scientific notation
Write decimal multiplier from given scientific notation
Write number from decimal multiplier
Write negative power of ten from given number
Write negative exponent of scientific notation
Write negative power of ten from given scientific notation
Write number from negative exponent
Compare two large numbers in scientific notation
Compare large quantities in context
Compare two small numbers in scientific notation
Compare small quantities in context
Compare one small number in scientific notation and one small number not in scientific notation
Compare one large number in scientific notation and one large number not in scientific notation
perform-mult-sp
perform-mult-r-sp
[SkillRule: Apply Exponent - whole; [Typein Skill: Apply Exponent]]
Choose table size
Enter heading in table
Enter product in table
Enter product from table with 3 sets of like terms
Enter product from table with 1 set of like terms
Enter product from table with 2 sets of like terms
Enter product from table with 0 sets of like terms
[SkillRule: Select Factor Quadratic; {factor quad eqn, right; factor quad eqn, left; fq expr; factor quad, denominator, left; factor quad, denominator, right}]
[SkillRule: factor-quadratic-pos-const; fq expr]
[SkillRule: factor-quadratic-coeff-one; fq expr]
Convert unit, multiplier
[SkillRule: Do Combine Terms - Whole; [Typein Skill: {Select Combine Terms; Combine like terms, no var}]]
Enter unit in denominator of unit conversion
Enter unit in numerator of unit conversion
Enter number in denominator of unit conversion
Enter number in numerator of unit conversion
Solve inequality with positive X coefficient
Solve inequality with negative X coefficient
[SkillRule: Isolate variable term in linear equation; {combine variables to right, sub; combine variables to right, add; combine variables to right, gen}]
[SkillRule: Isolate variable term in linear equation; {combine variables to left, sub; combine variables to left, add; combine variables to left, gen}]
[SkillRule: Eliminate Parens; {[var expr]/[const expr] = [const expr], multiply; [var expr]*[const expr] = [const expr], divide}]
[SkillRule: Make variable positive; {add x in -x; move neg var to other side}]
[SkillRule: Calculate negative coefficient; [Typein Skill: Make variable positive]]
Identify solution type of compound inequality using or
Join inequalities with or
Write solution as inequality - double
Write expression, initial and point
Write expression, initial and change
Write expression, two points
Identify solution type of compound inequality using and
Join inequalities with and
Represent ClosedPoint on numberline
Represent line on numberline
Represent OpenPoint on numberline
[SkillRule: Select Distribute; {Distribute binomials with radicals, numerator; Distribute binomials with radicals, denominator; Distribute -1 to remove parens in numerator; Distribute numerator before CLT, left; Distribute numerator before CLT, right; Distribute numerator term, left; Distribute numerator term, right}]
Solve absolute value greater than inequality
Solve absolute value less than or equal to inequality
Solve absolute value greater than or equal to inequality
Solve absolute value less than inequality
Entering x-intercept, SIF
Entering x-intercept, GLF
[SkillRule: Calculate Eliminate Parens; [Typein Skill: Eliminate Parens]]
Identify approximation as upper half
[SkillRule: done no solutions; {done no solution; Done No Solution, domain exception; Done No Solution, range exception}]
[SkillRule: factor-quadratic-neg-const; fq expr]
[SkillRule: factor-quadratic-coeff-not-one; fq expr]
[SkillRule: factor-difference-squares; fq expr]
[SkillRule: Factor difference of squares; {fq expr; factor quad, left; factor quad, right; factor quad, denominator, right; factor quad, denominator, left}]
[SkillRule: factor-common-factor; {factor common factor, quad; factor quad cf, left; factor quad cf, right; factor common factor, denominator}]
Enter number of desired outcomes from picture
Enter number of total outcomes from picture
Enter fractional probability of event
Enter decimal probability of event
Enter percent probability of event
Enter fractional probability of complementary event
Enter fractional probability of one
Enter decimal probability of complementary event
Enter decimal probability of one
Enter percent probability of complementary event
Enter percent probability of one
Enter number of desired outcomes from verbal description
Enter number of total outcomes from verbal description
Enter fractional probability of zero
Enter decimal probability of zero
Enter percent probability of zero
Enter fractional probability of disjoint events
Enter decimal probability of disjoint events
Enter percent probability of disjoint events
Enter theoretical probability
Enter experimental probability for small number
Enter experimental probability for medium number
Enter experimental probability for large number
Compare probabilities -- different trials
Compare probabilities -- same trials
Select dependence
Enter number of outcomes for event 1 in tool
Enter number of outcomes for event 2 in tool
Enter number of outcomes for event 1 in table
Enter number of outcomes for event 2 in table
Enter number of total outcomes in table
Select first event
Select second event
Enter sum of entries
Enter number of entries
Enter mean
Enter range
Enter median - odd entries
Select number of modes
Enter median - even entries
Enter single mode
Choose mean
Choose median
Choose mode
Enter original mean
Enter original median
Enter new mean
Enter new median
Compare means - removed outlier
Compare point and original mean - removed outlier
Compare medians - removed outlier
Compare differences - removed outlier
Compare means - added internal
Compare point and original mean - added internal
Compare medians - added internal
Compare differences - added internal
Compare means - added outlier
Compare point and original mean - added outlier
Compare medians - added outlier
Compare differences - added outlier
combine-like-terms-r-sp
[SkillRule: invert-mult; {ivm; ivm rational; general IVM catchall}]
Enter leg label
Enter hypotenuse label
Enter square of leg label
Enter square of hypotenuse label
Enter given leg in context
Find hypotenuse in context
Find square of given leg
Find square of hypotenuse
Enter given leg out of context
Find hypotenuse out of context
Find leg out of context
Enter given hypotenuse out of context
Find square of leg
Find square of given hypotenuse
Find leg in context
Enter given hypotenuse in context
Find length of vertical leg
Find length of horizontal leg
Find length using points in quadrant 1
Find coordinates of point C
Find length using points in quadrants 2 or 4
Find length using points in quadrant 3
Find length using points across quadrants
Find difference of x-coordinates
Find difference of y-coordinates
Find average of x-coordinates
Find average of y-coordinates
Find midpoint of horizontal or vertical line
Find midpoint using points in quadrants 2 or 4
Find midpoint using points in quadrant 3
Find midpoint using points across quadrants
Find midpoint using points in quadrant 1
Select first step
lcd-sp
Entered desired mean
Solve for negative data value
Determine that data value does not make sense - below range
Determine that mean does not make sense - below range
Solve for positive data value
Determine that data value does make sense
Determine that mean does make sense
Determine that data value does not make sense - above range
Determine that mean does not make sense - above range
[SkillRule: done infinite solutions; done infinite solutions]
Find slope from given graph
Enter point from given line
Enter point-slope form from given graph
Identify quadratic Parent Equation
Identify quadratic Parent Description
Identify quadratic Parent Curve
Edit Algebraic k in A problem
Choose Graphical k in A problem
Write expression, exponential
Entering a computed exponential value
Entering a given exponential value
Identify linear Parent Equation
Identify linear Parent Description
Identify linear Parent Curve
Choose Graphical k in G problem
Edit Algebraic k in G problem
Choose Graphical k in V problem
Edit Algebraic k in V problem
Choose Graphical refl-v in G problem
Edit Algebraic refl-v in G problem
Choose Graphical a in G problem
Edit Algebraic a in G problem
Choose Graphical refl-v in V problem
Edit Algebraic refl-v in V problem
Choose Graphical a in V problem
Edit Algebraic a in V problem
Edit Algebraic refl-v in A problem
Choose Graphical refl-v in A problem
Edit Algebraic a in A problem
Choose Graphical a in A problem
Edit Algebraic h in A problem
Choose Graphical h in A problem
Choose Graphical h in V problem
Edit Algebraic h in V problem
Choose Graphical h in G problem
Edit Algebraic h in G problem
Edit Algebraic refl-v in N problem
Choose Graphical refl-v in N problem
Edit Algebraic a in N problem
Choose Graphical a in N problem
Edit Algebraic h in N problem
Choose Graphical h in N problem
Edit Algebraic k in N problem
Choose Graphical k in N problem
Plot solution of greater than on number line
Plot solution of greater than or equal to on number line
Enter probability of first event
Enter probability of second event
Enter compound probability
[SkillRule: Do Multiply - Whole nested; [Typein Skill: Select Multiply/Divide, nested]]
[SkillRule: Do Multiply - Whole (typein-expression-2); [Typein Skill: Select Multiply]]
[SkillRule: Extract to consolidate vars - whole; [Typein Skill: Extract to consolidate vars]]
[SkillRule: Do Eliminate Parens - whole; [Typein Skill: Select Eliminate Parens]]
[SkillRule: Combine like terms; {clt nested, parens; CLT}]
[SkillRule: convert to log form; convert to log form]
[SkillRule: eval funcs; {eval funcs; eval funcs, const arg; eval funcs, const arg one side}]
[SkillRule: take log; take log]

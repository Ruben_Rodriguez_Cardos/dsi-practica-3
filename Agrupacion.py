import pandas as pd
import random


def main():
  print("Loading the file...")
  file = open('Data/SHORT_algebra_2008_2009_train.txt', "r") 
  aux = file.readlines()
  file.close()
  print("File loaded...")
  
  data = []
  for i in range(1,len(aux)):
    row = aux[i].split('\t')
    data.append([row[1],int(row[4]),int(row[14]), int(row[15]), int(row[16])])
    
  df = pd.DataFrame.from_records(data ,columns=["Id_Alumno","NVecesVisto","Incorrectos","Pistas","Correctos"])

  print("First Dataframe created!\n")  
  alumnos = df["Id_Alumno"].unique()

  data = []
  for i in range(1,len(alumnos)):
    print("Alumno: ", i," de ", str(len(alumnos)),"...\n")
    alumno = alumnos[i]
    aux = df.loc[df['Id_Alumno'] == alumno]
    data.append([alumno,aux["NVecesVisto"].mean(),aux["NVecesVisto"].std(),aux["Incorrectos"].mean(),aux["Incorrectos"].std(),aux["Pistas"].mean(),aux["Pistas"].std(),aux["Correctos"].mean(),aux["Correctos"].std()])
    
  df = pd.DataFrame.from_records(data ,columns=["Id_Alumno","NVecesVisto","STD_NVecesVisto","Incorrectos","STD_Incorrectos","Pistas","STD_Pistas","Correctos","STD_Correctos"])
  df.to_csv("Data/STUDENTS_algebra_2008_2009_train.csv", index=False)

	

if __name__=="__main__":
	main()